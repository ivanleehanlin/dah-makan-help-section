//
//  CustomCollectionViewController.h
//  DahMakanHelpSection
//
//  Created by Ivan Lee on 5/12/17.
//  Copyright © 2017 Ivan Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewController : UICollectionViewController
@property (nonatomic, strong) NSMutableArray* orders;

@end
