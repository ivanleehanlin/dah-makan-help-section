//
//  Order.h
//  DahMakanHelpSection
//
//  Created by Ivan Lee on 5/12/17.
//  Copyright © 2017 Ivan Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Order : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *orderLabel;
@property (strong, nonatomic) IBOutlet UILabel *arriveTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *paymentMethodLabel;

@end
