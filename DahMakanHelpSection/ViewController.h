//
//  ViewController.h
//  DahMakanHelpSection
//
//  Created by Ivan Lee on 5/12/17.
//  Copyright © 2017 Ivan Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *orderCollectionView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIView *collectionViewShadow;

@property (strong, nonatomic) IBOutlet UIView *helpCard;
@property (strong, nonatomic) IBOutlet UIView *helpCardShadow;

@property (strong, nonatomic) IBOutlet UIView *contactUsCard;
@property (strong, nonatomic) IBOutlet UIView *contactUsCardShadow;

@property (nonatomic,strong) NSArray *sliderImagesArray;
@property (nonatomic,strong) NSString *sliderIndexString;
@property (nonatomic,strong) NSString *currentPageIndexString;

@end

