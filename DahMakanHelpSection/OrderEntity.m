//
//  OrderEntity.m
//  DahMakanHelpSection
//
//  Created by Ivan Lee on 5/12/17.
//  Copyright © 2017 Ivan Lee. All rights reserved.
//

#import "OrderEntity.h"

@implementation OrderEntity

-(id) initWithOrderJson: (NSDictionary*) json {
    
    self = [super init];
    
    if (self) {
        self.orderId = [json[@"order_id"]stringValue];
        self.arrivesTime = [json[@"arrives_at_utc"]stringValue];
        self.paymentMethod = json[@"paid_with"];
    }
    return self;
}

-(NSDate*) getArriveDateTime {
    //todo convert timestring to date
    
    return [NSDate dateWithTimeIntervalSince1970:[self.arrivesTime doubleValue]];
}

-(NSString*) getDisplayTime {
    NSDateFormatter * formatter =  [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm aa"];
    NSString *dateString = [formatter stringFromDate:[self getArriveDateTime]];
    return dateString;
}

@end
