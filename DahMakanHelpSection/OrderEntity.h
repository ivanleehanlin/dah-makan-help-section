//
//  OrderEntity.h
//  DahMakanHelpSection
//
//  Created by Ivan Lee on 5/12/17.
//  Copyright © 2017 Ivan Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderEntity : NSObject

@property (strong, nonatomic) NSString *orderId;
@property (strong, nonatomic) NSString *arrivesTime;
@property (strong, nonatomic) NSString *paymentMethod;

-(NSDate*) getArriveDateTime;
-(NSString*) getDisplayTime;
-(id) initWithOrderJson: (NSDictionary*) json;

@end
