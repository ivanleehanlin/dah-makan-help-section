//
//  SortingUtils.h
//  DahMakanHelpSection
//
//  Created by Ivan Lee on 5/13/17.
//  Copyright © 2017 Ivan Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SortingUtils : NSObject

+ (NSMutableArray*) sortArray: (NSMutableArray*)array WithKey:(NSString*) key;

@end
