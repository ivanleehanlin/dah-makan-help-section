//
//  ViewController.m
//  DahMakanHelpSection
//
//  Created by Ivan Lee on 5/12/17.
//  Copyright © 2017 Ivan Lee. All rights reserved.
//

#import "ViewController.h"
#import <AFHTTPSessionManager.h>
#import <SVProgressHUD.h>
#import "OrderEntity.h"
#import "Order.h"
#import "SortingUtils.h"

#define REST_URL @"http://staging-api.dahmakan.com/test/orders"

@interface ViewController ()

@property(strong, nonatomic) NSMutableArray<OrderEntity*> *orderArray;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initOrderCollectionView];
    [self initHelpCard];
    [self initContactUsCard];
    
    [self getServerData];
    
    
}

#pragma rest request
- (void) getServerData {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"application/json", nil];
    
    [SVProgressHUD showWithStatus:@"Contacting server..."];
    [manager GET:REST_URL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [SVProgressHUD dismiss];
        NSLog(@"response%@",responseObject);
        NSArray* orders = responseObject[@"orders"];
        [self updateOrders:orders];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [SVProgressHUD dismiss];
        [self restRequestErrorPopupWithTitle:@"Unable to reach server" andMessage:[error localizedDescription]];
    }];
}

#pragma collection view
- (void) initOrderCollectionView {
    
    [self.orderCollectionView.layer setCornerRadius:5.0f];
    [self.orderCollectionView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.orderCollectionView.layer setBorderWidth:0.2f];
    
    [self.collectionViewShadow.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
    [self.collectionViewShadow.layer setShadowOpacity:0.5];
    [self.collectionViewShadow.layer setShadowRadius:5.0];
    [self.collectionViewShadow.layer setShadowOffset:CGSizeMake(10.0f, 10.0f)];
    self.collectionViewShadow.layer.zPosition = -1;
    
    self.orderCollectionView.dataSource = self;
    self.orderCollectionView.delegate = self;
    self.orderCollectionView.bounces = NO;
    self.orderCollectionView.alwaysBounceVertical = NO;
    self.orderCollectionView.alwaysBounceHorizontal = NO;
    self.orderCollectionView.showsVerticalScrollIndicator = NO;
    self.orderCollectionView.scrollEnabled = YES;
    self.orderCollectionView.pagingEnabled = YES;
    
    [self.orderCollectionView registerNib:[UINib nibWithNibName:@"OrderCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
}

- (void) initHelpCard {
    [self.helpCard.layer setCornerRadius:5.0f];
    [self.helpCard.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.helpCard.layer setBorderWidth:0.2f];
    
    [self.helpCardShadow.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
    [self.helpCardShadow.layer setShadowOpacity:0.5];
    [self.helpCardShadow.layer setShadowRadius:5.0];
    [self.helpCardShadow.layer setShadowOffset:CGSizeMake(10.0f, 10.0f)];
    self.helpCardShadow.layer.zPosition = -1;
}

- (void) initContactUsCard {
    [self.contactUsCard.layer setCornerRadius:5.0f];
    [self.contactUsCard.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.contactUsCard.layer setBorderWidth:0.2f];
    
    [self.contactUsCardShadow.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
    [self.contactUsCardShadow.layer setShadowOpacity:0.5];
    [self.contactUsCardShadow.layer setShadowRadius:5.0];
    [self.contactUsCardShadow.layer setShadowOffset:CGSizeMake(10.0f, 10.0f)];
    self.contactUsCardShadow.layer.zPosition = -1;
}

- (void) updateOrders:(NSArray*)orders {
    
    self.orderArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary* orderJson in orders) {
        OrderEntity* order = [[OrderEntity alloc]initWithOrderJson:orderJson];
        [self.orderArray addObject:order];
    }
    
    self.orderArray = [SortingUtils sortArray:self.orderArray WithKey:@"arrivesTime"];
    [self.orderCollectionView reloadData];
    self.pageControl.numberOfPages = self.orderArray.count;
}

#pragma alert quick access
- (void) restRequestErrorPopupWithTitle: (NSString*) title andMessage: (NSString*) message  {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert ]; //instantiate an alertviewcontroller
    
    UIAlertAction *tryAgainButton = [UIAlertAction actionWithTitle:@"Try Again" style:UIAlertActionStyleDefault handler: ^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
        [self getServerData];
    }];
    
    [alert addAction:tryAgainButton];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma scroll view delegate impl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.orderCollectionView.frame.size.width;
    self.pageControl.currentPage = self.orderCollectionView.contentOffset.x / pageWidth;
}

#pragma collection view delegate impl
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderEntity* selectedOrder = [self.orderArray objectAtIndex:indexPath.item];
    
    NSLog(@"%@%@", @"Selected item id is ", selectedOrder.orderId);
    //todo prepare segue
    
}

#pragma collection view source impl
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.orderArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Order *cell = (Order*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Cell" owner:self options:nil];
        cell = [nib objectAtIndex:indexPath.item];
    }
    
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    if (self.orderArray.count !=0) {
        if (self.orderArray[indexPath.item] != nil)
        {
            cell.orderLabel.text = [self.orderArray objectAtIndex:indexPath.item].orderId;
            cell.arriveTimeLabel.text = [[self.orderArray objectAtIndex:indexPath.item] getDisplayTime];
            cell.paymentMethodLabel.text = [self.orderArray objectAtIndex:indexPath.item].paymentMethod;
            
        }
    }else{
        cell.orderLabel.text = @"no order yet";
    }
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
