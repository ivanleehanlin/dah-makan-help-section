//
//  SortingUtils.m
//  DahMakanHelpSection
//
//  Created by Ivan Lee on 5/13/17.
//  Copyright © 2017 Ivan Lee. All rights reserved.
//

#import "SortingUtils.h"

@implementation SortingUtils

+ (NSMutableArray*) sortArray: (NSMutableArray*)array WithKey:(NSString*) key {
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
    
    NSArray *orderedArray = [array sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSMutableArray *output = [[NSMutableArray alloc]init];
    [output addObjectsFromArray:orderedArray];
    
    return output;
    
}
@end
